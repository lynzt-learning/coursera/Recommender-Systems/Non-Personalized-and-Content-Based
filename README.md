# python container
docker build -t py/rec .

# run script
docker run -it --rm \
  -v "$PWD":/usr/src/app \
  py/rec python3 ./script.py
