import pandas as pd

movies = pd.read_csv('./week2/nonpers-assignment/data/movies.csv', encoding='ISO-8859-1')
ratings = pd.read_csv('./week2/nonpers-assignment/data/ratings.csv', encoding='ISO-8859-1')
tags = pd.read_csv('./week2/nonpers-assignment/data/tags.csv', encoding='ISO-8859-1')

def MeanItemBasedItemRecommender():
    mean_ratings = ratings.groupby(['movieId'])['rating'].mean().sort_values(ascending=False).to_frame()
    return pd.merge(mean_ratings, movies, on='movieId').rename({'rating':'mean_rating'}, axis=1)


def DampedItemMeanModelProvider():
    alpha = 5
    global_mean = ratings['rating'].mean()
    ratings_sum = ratings.groupby(['movieId'])['rating'].sum()
    ratings_count = ratings.groupby(['movieId'])['rating'].count()

    print (ratings_count.head())

    ratings_damped_mean = ((ratings_sum + (global_mean * alpha)) / (ratings_count + alpha)).sort_values(ascending=False).to_frame()
    return pd.merge(ratings_damped_mean, movies, on='movieId').rename({'rating':'damped_mean_rating'}, axis=1)


recs1 = MeanItemBasedItemRecommender()
recs2 = DampedItemMeanModelProvider()

merged_df = recs1.merge(recs2, how='left', on=['movieId', 'title'])[['movieId', 'title', 'mean_rating', 'damped_mean_rating']].head(20)
print (merged_df.to_string())
